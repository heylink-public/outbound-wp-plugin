<?php

/**
 * Plugin Name: Heylink Tracking
 * Plugin URI: https://heylink.com
 * Description: Make every click count - automated affiliate link handling for your site.
 * Author: heylink.com
 * Version: 2.4.2
 * Author URI: mailto:support@heylink.com 
 * @package WordPress
 */

/**
 * Main plugin class. Loads assets, sets variables, loads dependencies.
 * 
 * Load options, set class variables, require depenedencies, trigger OB cache or WordPress hooks,
 * load Heylink .js script, register actiavation, deactivation hooks.
 *
 * @todo autoload
 * @todo namespace
 * 
 * @package WordPress
 * @subpackage Heylink Tracking
 */
class Heylink_Plugin
{

	/**
	 * Used only for translation reference. Shouldn't be used directly.
	 * @see: https://developer.wordpress.org/reference/functions/__/#comment-540
	 * Use the exact same text, whenever a function accepts text_domain as a parameter.
	 * 
	 * @var string 
	 */
	var $text_domain = 'heylink-tracking';

	/**
	 * Options page suffix as it appears under admin area of the website
	 * 
	 * @var string
	 */
	const OPTIONS_PAGE_SUFFIX = 'heylink-tracker';

	/**
	 * Options database name. 
	 * Stored in _options database table of WordPress install.
	 * 
	 * @const string
	 */
	const OPTIONS_NAME = 'heylink_tracker_plugin_options';

	/**
	 * Options variable. Store the loaded options.
	 * 
	 * @var MIXED
	 */
	public $options;

	/**
	 * Heylink tracking URL to send requests to.
	 * 
	 * @var string
	 */
	public $heylink_tracking_url;

	/**
	 * API key. Currently uses channels key.
	 * 
	 * @var string
	 */
	public $api_key;

	/**
	 * Hash hmac method
	 * 
	 * @var string
	 */
	public $hash_hmac_method = 'ripemd160';

	/**
	 * URL parameter name for encoded url, associated with go
	 * 
	 * @var string
	 */
	public $url_param = 'url';

	/**
	 * URL parameter name for pageUrl, associated with go
	 * 
	 * @var string 
	 */
	public $page_url_param = 'pageUrl';

	/**
	 * URL parameter for targetUrl
	 * 
	 * @var string
	 */
	public $target_url_param = 'targetUrl';

	/**
	 * URL parameter name for the hashed value of the url
	 * 
	 * @var type 
	 */
	public $hash_url_param = 'abc';

	/**
	 * Hash hmac secret key
	 * 
	 * @var string
	 */
	public $hash_hmac_key = 'secretkey';

	/**
	 * Load plugin options, set class variables, require dependencies, add redirect, head, footer hooks
	 * based on user (options defined settings, that can be by default found at:
	 * wp-admin/admin.php?page=heylink-tracker
	 */
	public function __construct()
	{

		$this->options = get_option(Heylink_Plugin::OPTIONS_NAME);

		// as this option is by default hidden under debug mode
		// make sure it's set to 1, unless otherwise set
		if (empty($this->options['ext_links'])) {
			$this->options['ext_links'] = 1;
		}

		$this->heylink_tracking_url = 'https://api.heylink.com/tracking/clicks/v1';
		$this->api_key = $this->options['api_key'];

		// require related classes
		require_once(__DIR__ . DIRECTORY_SEPARATOR . 'includes' . DIRECTORY_SEPARATOR . 'class_heylink_ajax.php');
		require_once(__DIR__ . DIRECTORY_SEPARATOR . 'includes' . DIRECTORY_SEPARATOR . 'class_heylink_helper.php');
		require_once(__DIR__ . DIRECTORY_SEPARATOR . 'includes' . DIRECTORY_SEPARATOR . 'class_heylink_settings_main.php');
		require_once(__DIR__ . DIRECTORY_SEPARATOR . 'includes' . DIRECTORY_SEPARATOR . 'class_heylink_settings_metaboxes.php');

		require_once(__DIR__ . DIRECTORY_SEPARATOR . 'includes' . DIRECTORY_SEPARATOR . 'class_heylink_api.php');

		// require updates plugin
		require_once(__DIR__ . DIRECTORY_SEPARATOR . 'class_heylink_update_checker.php');

		// add redirect hooks
		add_action('template_redirect', array($this, 'init_redirect'));

		// add WordPress hooks
		if (isset($this->options['method_hooks']) && $this->options['method_hooks'] != '' &&  isset($this->options['api_key']) && $this->options['api_key'] != '') {
			//add_action('init', array($this, 'heylink_init_hook'));

			// Adds redirect for all extenral redirects.
			add_filter('wp_redirect', array($this, 'heylink_tracker_redirect'), 999);
		}


		// conditionally load .js script
		if (
			isset($this->options['method_js']) && $this->options['method_js'] != '' && isset($this->options['api_key']) && $this->options['api_key'] != ''
		) {
			add_action('wp_enqueue_scripts', array($this, 'add_js_to_footer'));
		}

		// @todo maybe move to a class method instead of anonymous function
		add_filter('script_loader_tag', function ($tag, $handle) {
			if ('heylink_js_tracker' !== $handle) {
				return $tag;
			}

			return str_replace(' src', ' defer src', $tag); // defer the script
			//return str_replace( ' src', ' async src', $tag ); // OR async the script
			//return str_replace( ' src', ' async defer src', $tag ); // OR do both!
		}, 10, 2);

		register_activation_hook(__FILE__, array($this, 'heylink_on_activation'));
		add_action('activated_plugin', array($this, 'heylink_tracker_activation_redirect'));
	}

	/**
	 * When a user lands on a specific, options defined URL say /go make a number of checks and redirect user to 
	 * the provided URL through wrapping method.
	 * 
	 * @hook template_redirect
	 */
	public function init_redirect()
	{
		$look_for = @$this->options['ext_links_path'];

		$protocol = strtolower(substr($_SERVER["SERVER_PROTOCOL"], 0, 5)) == 'https' ? 'https' : 'http';
		$hostname = $_SERVER['HTTP_HOST'];
		$url = $protocol . '://' . $hostname . '/' . $look_for . '?' . $this->url_param . '=';

		$actual_link = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";

		if (isset($_GET[$this->url_param]) && strpos($actual_link, $look_for) !== false) {
			$target_url = urlencode(htmlspecialchars_decode(html_entity_decode(urldecode($_GET[$this->url_param]))));

			// hash parameter
			if (isset($_GET[$this->hash_url_param])) {
				$encoded_url = $_GET[$this->url_param]; // todo 
				$hash_expected = hash_hmac($this->hash_hmac_method, $encoded_url, $this->hash_hmac_key);
				$hash_value = esc_attr($_GET[$this->hash_url_param]);
				if (!Heylink_Helper::validate_hash($hash_expected, $hash_value)) {
					status_header(404);
					return;
				}
			} else {
				status_header(404);
				return;
			}

			// custom actions with Pretty Links link found
			if (isset($_GET['pri_link']) and esc_attr($_GET['pri_link']) === 'true') {
				global $prli_custom_redirect_url;
				add_action('prli_before_redirect', array($this, 'custom_prli_redirect'));
				$this->pretty_permalinks_actions();
				remove_action('prli_before_redirect', array($this, 'custom_prli_redirect'));
			}

			if (isset($this->options['api_key']) and !empty($this->options['api_key'])) {
				$url = $this->heylink_tracking_url . '/' . $this->options['api_key'] . '?targetUrl=' . $target_url;
				wp_redirect($url);
				exit;
			}
		}
	}

	/**
	 * A custom redirect method hook used only if Pretty Links are enabled.
	 * 
	 * @hook prli_before_redirect
	 * @global string $prli_custom_redirect_url URL to redirect to
	 * @return null
	 */
	public function custom_prli_redirect()
	{
		global $prli_custom_redirect_url;
		wp_redirect($prli_custom_redirect_url);
		exit;
	}

	/**
	 * Trigger Pretty Links actions to work with the plugin. 
	 * Coode fetched from PrliAPpController.php, may need some testing.
	 * 
	 * @todo test this in different case scenarios on a site with PrettyPermalinks installed and activated
	 * 
	 * @global object $prli_link
	 * @global object $prli_utils
	 * @see PrliAppController.php from pretty-link plugin
	 */
	public function pretty_permalinks_actions()
	{
		global $prli_link, $prli_utils, $prli_custom_redirect_url;
		$parsed_link = parse_url($_GET['url']);
		$parsed_link_path = $prli_link->is_pretty_link($parsed_link['path'], false);
		$link = $prli_link->getOneFromSlug(rawurldecode($parsed_link_path['pretty_link_found']->slug));
		$prli_custom_redirect_url = $this->heylink_tracking_url . '/' . $this->options['api_key'] . '?targetUrl=' . $link->url;
		// original pretty-link code, may need to be improved for security needs
		$custom_get = $_GET;
		$prli_utils->track_link($link->slug, $custom_get);
	}

	/**
	 * Enqueue heylink script.js with the correct API key if needed.
	 * 
	 * @hook wp_enqueue_scripts
	 */
	public function add_js_to_footer()
	{

		if (Heylink_Helper::is_site_whitelisted() and Heylink_Helper::is_current_entry_whitelisted()) {
			$src = "https://tag.heylink.com/" . $this->options['api_key'] . "/script.js";
			wp_enqueue_script("heylink_js_tracker", $src, null, null, true);
		}

		// @todo maybe remove this if
		if (!Heylink_Helper::is_site_whitelisted()) :
			if ((!Heylink_Helper::is_current_entry_blacklisted() and Heylink_Helper::is_site_blacklisted()) or !Heylink_Helper::is_site_blacklisted()) :
				$src = "https://tag.heylink.com/" . $this->options['api_key'] . "/script.js";
				wp_enqueue_script("heylink_js_tracker", $src, null, null, true);
			endif;
		endif;
	}

	/**
	 * Filter provided content (buffer) - find occurences of all links and replace their href attribute 
	 * in a number of case scenarios.
	 * 
	 * @todo reduce the method length, improve the code
	 */
	public function heylink_tracker_redirect($location)
	{
		 
		if(
			isset($this->options['method_hooks']) && 
			$this->options['method_hooks'] != '' &&  
			isset($this->options['api_key']) && 
			$this->options['api_key'] != '' && 
			Heylink_Helper::is_external_link($location) &&
			strpos($location, "heylink") == false
			) {
			
			$current_url = home_url($wp->request);
			$modified_link = Heylink_Helper::modify_link(
				$location,
				$current_url,
				"2", // $this->options['ext_links']
				$this->options['ext_links_path'],
				$this->url_param,
				$this->page_url_param,
				$this->target_url_param,
				$this->hash_url_param,
				$this->hash_hmac_method,
				$this->hash_hmac_key,
				$this->heylink_tracking_url,
				$this->api_key,
			);

			return $modified_link;
		} else {
			return $location;
		}
		
	}
	/**
	 * Filter provided content (buffer) - find occurences of all links and replace their href attribute 
	 * in a number of case scenarios.
	 * 
	 * @todo reduce the method length, improve the code
	 */
	public function heylink_tracker($buffer)
	{
		global $wp;

		// get the currently viewed url, with respect to where WordPress is installed
		$current_url = home_url($wp->request);

		$doc = new DOMDocument();

		// On rare occasion some sites do not send a buffer, causing errors. Return if this happens.
		if (!$buffer) {
			return;
		} // If no buffer, return nothing - This fixed an issues with WordPress Hooks function not working on rare ocations.

		$loadHTMLbuffer = mb_convert_encoding($buffer, 'HTML-ENTITIES', get_bloginfo("charset")); // Fixes chanrset on content
		$doc->loadHTML($loadHTMLbuffer, LIBXML_HTML_NOIMPLIED | LIBXML_HTML_NODEFDTD | LIBXML_NOWARNING | LIBXML_NOERROR);
		$links = $doc->getElementsByTagName('a');

		if (!empty($links)) {
			foreach ($links as $link) {
				$link_href =  $link->getAttribute('href');
				
			}
		}

		$buffer = $doc->saveHTML();

		return htmlspecialchars_decode($buffer);
	}


	/**
	 * Filter content and excerpt data in a similar way to output buffering, although
	 * covering only inner content of any given entry, say post, page.
	 * 
	 * @hook init
	 */
	public function heylink_init_hook()
	{
		add_filter('the_content', array($this, 'heylink_tracker'), -99999);
		add_filter('the_excerpt', array($this, 'heylink_tracker'), -99999);
	}

	/**
	 * Plugin activation hook. Setup default plugin options and save them in the database.
	 *
	 * @hook register_activation_hook
	 */
	public function heylink_on_activation()
	{
		// @todo add new options if any once in a while
		$default = array(
			'ext_links_path' => 'go',
			'rel_nofollow' => '1',
			'method_js' => '1',
			'method_hooks' => '0',
			'method_ob' => '0',
			'ext_links' => '2',
			'blacklist_site' => '0',
			'whitelist_site' => '0',
		);

		add_option(Heylink_Plugin::OPTIONS_NAME, $default);
		update_option(Heylink_Plugin::OPTIONS_NAME, $default);
	}

	/**
	 * Redirect to settings/options page page after plugin activation.
	 * 
	 * @hook activated_plugin
	 */
	public function heylink_tracker_activation_redirect($plugin)
	{
		if ($plugin == plugin_basename(__FILE__)) {
			exit(wp_redirect(esc_url(add_query_arg(
				'page',
				Heylink_Plugin::OPTIONS_PAGE_SUFFIX,
				get_admin_url() . 'admin.php'
			))));
		}
	}

}

$heylink_plugin_instance = new Heylink_Plugin();