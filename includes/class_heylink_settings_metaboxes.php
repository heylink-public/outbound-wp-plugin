<?php

/**
 * A class that adds a number of configurable plugin to different parts of the website
 * associated with specific post or page.
 * 
 * @package WordPress
 * @subpackage Heylink Tracking
 */
class Heylink_Settings_Metaboxes {

	/**
	 * An array of keys where the metabox shouldn't display
	 * 
	 * @var ARRAY_A
	 */
	public $exclusions;

	public function __construct() {

		// @todo add new elements here upon discovery
		$this->exclusions = array(
				'attachment',
				'revision',
				'nav_menu_item',
				'custom_css',
				'customize_changeset',
				'oembed_cache',
				'wp_block',
				'wp_template',
				'wp_template_part',
				'wp_global_styles',
				'wp_navigation',
				'pretty-link',
				'acf-field-group'
		);

		add_action( 'add_meta_boxes', array( $this, 'add_meta_box' ) );
		add_action( 'save_post', array( $this, 'save_postdata' ) );
		add_action( 'admin_head', array( $this, 'admin_custom_styles_scripts' ) );
	}

	/**
	 * Create a meta box place at the side of each post / page / cpt
	 * to create custom overwrite rules
	 * 
	 * @hook add_meta_boxes
	 */
	public function add_meta_box() {

		$registered_post_types = get_post_types();

		$options = get_option( Heylink_Plugin::OPTIONS_NAME );

		if ( isset( $options['blacklist_whitelist_site'] ) and
						(
						$options['blacklist_whitelist_site'] == 'blacklist'
						or
						$options['blacklist_whitelist_site'] == 'whitelist'
						)
		):
			add_meta_box(
							'heylink_settings',
							__( 'Heylink Options', 'heylink-tracking' ),
							array( $this, 'show_heylink_box' ),
							array_diff( $registered_post_types, $this->exclusions ),
							'side',
							'high'
			);
		endif;
	}

	/**
	 * Add a metabox to the post / page / cpt with Blacklist / Whitelist
	 * 
	 * @todo use class variables instead of hardcoded strings
	 * 
	 * @hook add_meta_box
	 */
	public function show_heylink_box() {
		global $post;
		$options = get_option( Heylink_Plugin::OPTIONS_NAME );
		
		if( isset( $options['blacklist'] ) ) {
			$blacklist_check = in_array( $post->ID, $options['blacklist'] );
		} else {
			$blacklist_check = 0;
		}
		
		if( isset( $options['whitelist'] ) ) {
			$whitelist_check = in_array( $post->ID, $options['whitelist'] );
		} else {
			$whitelist_check = 0;
		}
		
		wp_nonce_field( 'heylink_hook_check_save', 'heylink_hook_nonce' );
		?>
		<?php
		if ( isset( $options['blacklist_whitelist_site'] ) and
						(
						$options['blacklist_whitelist_site'] == 'blacklist'
						or
						$options['blacklist_whitelist_site'] == 'whitelist'
						) ):
			?>
			<div class="heylink-sidebar-wrap">
			<?php if ( $options['blacklist_whitelist_site'] == 'blacklist' ): ?>
						<div class="heylink-meta-box">
								<input
										type="checkbox"
										class="ui-toggle components-checkbox-control__input heylink-checkbox"
										name="blacklist_check"
										id="blacklist-check"
										value="1"
										<?php checked( $blacklist_check, 1 ) ?>
										>
								<span class="checkmark checkmark-blacklist"></span>
								<label for="blacklist-check"><?php _e( 'Blacklist', 'heylink-tracking' ); ?></label>
								<p class="heylink-meta-box__desc"><?php _e( 'With a globally checked blacklist, if checked, the page will not modify the links.', 'heylink-tracking' ); ?></p>
						</div>
			<?php endif; ?>

			<?php if ( $options['blacklist_whitelist_site'] == 'whitelist' ): ?>
						<div class="heylink-meta-box">
								<input
										type="checkbox"
										class="ui-toggle components-checkbox-control__input heylink-checkbox"
										name="whitelist_check"
										id="whitelist-check"
										value="1"
										<?php checked( $whitelist_check, 1 ) ?>
										>
								<span class="checkmark checkmark-whitelist"></span>
								<label for="whitelist-check"><?php _e( 'Whitelist', 'heylink-tracking' ); ?></label>
								<p class="heylink-meta-box__desc"><?php _e( 'With a globally checked whitelist, if checked, the page will modify the links, ignoring all other settings.', 'heylink-tracking' ); ?></p>
						</div>
			<?php endif; ?>
			</div>
		<?php endif; ?>
		<?php
	}

	/**
	 * Validate and save custom settings of any given post or page or cpt.
	 * 
	 * @hook save_post
	 * 
	 * @param int $post_id
	 */
	public function save_postdata( $post_id ) {

		// Check if our nonce is set.
		if ( !isset( $_POST['heylink_hook_nonce'] ) && !wp_verify_nonce( $_POST['heylink_hook_nonce'], 'heylink_hook_check_save' ) ) {
			return;
		}
		
		$options = get_option( Heylink_Plugin::OPTIONS_NAME );

		$blacklist_check = (int) filter_var( $_POST['blacklist_check'] ?? 0, FILTER_SANITIZE_NUMBER_INT );
		
		if( isset( $options['blacklist'] ) and !empty( $options['blacklist'] ) and $blacklist_check ) {
			if ( !in_array( $post_id, $options['blacklist'] ) ) {
						$options['blacklist'][] = $post_id;
				} else {
						$options['blacklist'] = array_diff( $options['blacklist'], array( $post_id ) );
				}
		} else {
				$options['blacklist'] = array( $post_id );
		}
		
		if( !$blacklist_check and isset( $options['blacklist'] ) and !empty( $options['blacklist'] ) ) {
			 $options['blacklist'] = array_diff( $options['blacklist'], array( $post_id ) );
		}

		$whitelist_check = (int) filter_var( $_POST['whitelist_check'] ?? 0, FILTER_SANITIZE_NUMBER_INT );

		if( isset( $options['whitelist'] ) and !empty( $options['whitelist'] ) and $whitelist_check ) {
			if ( $whitelist_check and !in_array( $post_id, $options['whitelist'] ) ) {
						$options['whitelist'][] = $post_id;
				} else {
						$options['whitelist'] = array_diff( $options['whitelist'], array( $post_id ) );
				}
		} else {
				$options['whitelist'] = array( $post_id );
		}
		
		if( !$whitelist_check and isset( $options['whitelist'] ) and !empty( $options['whitelist'] ) ) {
			 $options['whitelist'] = array_diff( $options['whitelist'], array( $post_id ) );
		}

		update_option( Heylink_Plugin::OPTIONS_NAME, $options );
	}

	/**
	 * Overwrite custom meta box styles with inline 
	 * CSS and JS injected into the admin part of the head.
	 * 
	 * @hook admin_head
	 */
	public function admin_custom_styles_scripts() {
		// https://www.toptal.com/developers/cssminifier
		// @todo disable drag and drop from the .js
		?>
		<style type="text/css">
				#heylink_settings .handle-order-higher,#heylink_settings .handle-order-lower{display:none!important}#heylink_settings h2{position:relative!important;padding:16px 48px 16px 16px!important;outline:0!important;width:100%!important;font-weight:500!important;text-align:left!important;color:#1e1e1e!important;font-size:13px!important}#heylink_settings .heylink-checkbox:checked{background-color:#007cba!important;border-color:#007cba!important;color:#fff}#heylink_settings .inside{padding:0 48px 16px 16px!important}#heylink_settings .heylink-meta-box{margin:10px 0 20px}#heylink_settings .heylink-meta-box input{margin-right:7px}.heylink-meta-box{position:relative}.heylink-meta-box .checkmark{position:absolute;top:0;left:0;height:25px;width:25px;display:none;cursor:pointer}.heylink-meta-box input:checked~.checkmark,.heylink-meta-box input:checked~.checkmark:after{display:block}.heylink-meta-box .checkmark:after{content:"";position:absolute;display:none;left:7px;top:2px;width:5px;height:10px;border:solid #fff;border-width:0 3px 3px 0;-webkit-transform:rotate(45deg);-ms-transform:rotate(45deg);transform:rotate(45deg)}.heylink-meta-box__desc, .heylink-setttings__desc{margin-top:5px!important}
				.heylink-setttings__desc {font-style: italic;} .heylink-textarea {max-width: 50%; min-height: 100px;} .c-settings__heading { font-size: 14px; margin: 10px 0 0 0 !important; } .c-list { margin: 5px 0 0 0 !important;} .heylink-notice {margin-left: 0;margin-top: 20px;} .heylink-select {min-width: 200px;}
		</style>
		<script type="text/javascript">
		  jQuery(document).ready(function () {
		    var c = jQuery(".checkmark-blacklist"), i = jQuery(".checkmark-whitelist");
		    jQuery(c).click(function () {
		      console.log("a"), jQuery("#blacklist-check").trigger("click")
		    }), jQuery(i).click(function () {
		      jQuery("#whitelist-check").trigger("click")
		    })
		  });
		</script>
		<?php
		$current_page_param = '';
		
		if( isset( $_GET['page'] ) ){
			$current_page_param = esc_attr( $_GET['page'] );
		}
		
		if ( $current_page_param == Heylink_Plugin::OPTIONS_PAGE_SUFFIX ):
			?>
			<link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
			<script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>
			<script type="text/javascript">
			  jQuery(document).ready(function () {
			      jQuery('.heylink-select').select2({
			          ajax: {
			              url: ajaxurl,
			              dataType: 'json',
			              delay: 250,
			              data: function (params) {
			                  var query = {
			                      search: params.term,
			                      action: '<?php echo Heylink_AJAX::AJAX_ENDPOINT; ?>'
			                  }
			                  return query;
			              }
			          },
			          placeholder: '<?php _e( 'Search for entries.', 'heylink-tracking' ); ?>',
			          minimumInputLength: 3,
			      });
			  });
			</script>
			<?php
		endif;
	}

}

$heylink_settings_metaboxes_instance = new Heylink_Settings_Metaboxes();





